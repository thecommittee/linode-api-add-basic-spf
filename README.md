## Synopsis

Script to remove junk spf records and create a basic proper spf record via the Linode API.

## Motivation

I had a lot of broken spf records that needed to be cleaned up across many domains. I needed a script to the the bulk of the easy work for me.  I call this in a loop to parse a list.


## API Reference

    usage: linode-spf-add -g -d <domain name>
           g = google apps include

## Tests

Needs Tests

## Contributors

Accepting pull requests

## License